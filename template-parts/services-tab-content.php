<div class="services-content__text">
    <?php the_content(); ?>
</div>
<?php
$rows = get_field('service_gallery_switch');
if( $rows ): ?>
  <div class="services-content__info">
    <div class="row">
      <div class="services-tabs col-md-7">
        <?php foreach($rows as $key => $value): ?>
          <button class="services-tabs__item <?php echo ($key === 0) ? 'active' : ''?>" data-tab="service-id-<?= $key; ?>" data-type="<?php echo get_post_type(); ?>"><?php echo $value['service_gallery_switch_service_name']; ?>;</button>
        <?php endforeach; ?>
      </div>
      <?php foreach($rows as $key => $value): ?>
        <div class="services-tabs-content col-md-4 ml-auto <?php echo ($key === 0) ? 'active' : ''?>" id="service-id-<?= $key; ?>">
          <?php
            if( $value['service_gallery_switch_slider'] ):
          ?>
            <div class="slick-carousel">
              <?php foreach( $value['service_gallery_switch_slider'] as $image ): ?>
                <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" style="width: 100%"/>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
<?php endif; ?>