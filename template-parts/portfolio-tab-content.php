<?php
/**
 * Created by PhpStorm.
 * User: dnnsys
 * Date: 05.02.2018
 * Time: 12:38
 */
?>
<div class="portfolio-content__text">
    <?php the_content(); ?>
</div>
<?php
$rows = get_field('portfolio_address');
if( $rows ): ?>
    <div class="portfolio-content__info">
        <div class="row">
            <div class="portfolio-address-tabs col-md-3">
                <div class="portfolio-content__address-title">Адреса проведения работ:</div>
                <?php foreach($rows as $key => $value): ?>
                    <button class="portfolio-address-tabs__item <?php echo ($key === 0) ? 'active' : ''?>" data-tab="portfolio-id-<?= $key; ?>" data-type="<?php echo get_post_type(); ?>"><?php echo $value['field_address']; ?>;</button>
                <?php endforeach; ?>
            </div>
            <?php foreach($rows as $key => $value): ?>
              <div class="portfolio-tabs-content col-md-9 <?php echo ($key === 0) ? 'active' : ''?>" id="portfolio-id-<?= $key; ?>">
                  <? echo do_shortcode($value["gallery_shortcode"]); ?>
              </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
