<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AmStroy
 */

?>

	</div><!-- #content -->

	<footer id="footer" class="footer">
    <div class="container">
      <div class="footer__inner">
        <div class="footer__left">
          <div class="footer__logo">
              <?php the_custom_logo(); ?>
          </div>
          <div class="footer__docs">
            <span><?= get_option('company_name'); ?></span>
            <span>ОГРН: 239472934</span>
            <span>ИНН: 2938402983</span>
            <span>БИК: 2394879387</span>
          </div>
        </div>
        <div class="footer__right">
          <div class="footer__developer">
            <div class="footer__developer-info">
              Сайт разработан компанией:<br/>
              <a href="#">Контора интернет-проектов</a>
            </div>
            <div class="footer__developer-logo">
              <img src="<?= get_template_directory_uri() . '/images/dev-logo.png' ?>" alt="Developer">
            </div>
          </div>
        </div>
      </div>
    </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<?php if (!file_exists( __FILE__ . 'build/')) : ?>
  <script src="http://localhost:3000/js/app.js"></script>
<?php endif; ?>
</body>
</html>
