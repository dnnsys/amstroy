<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AmStroy
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wptime-plugin-preloader"></div>
<div id="page" class="site">
	<header id="header" class="header">
    <div class="container">
        <div class="header__inner">
          <div class="header__logo">
              <?php the_custom_logo(); ?>
          </div>

          <nav id="site-navigation" class="header__navigation">
            <ul class="main-nav">
              <li class="main-nav__item">
                <a href="#" class="main-nav__link">О компании</a>
              </li>
              <li class="main-nav__item">
                <a href="#" class="main-nav__link">Услуги</a>
              </li>
              <li class="main-nav__item">
                <a href="#" class="main-nav__link">Портфолио</a>
              </li>
              <li class="main-nav__item">
                <a href="#" class="main-nav__link">Контакты</a>
              </li>
            </ul>
          </nav><!-- #site-navigation -->
          <div class="header__phone">
            <span class="header__phone-code"><?php echo get_option('site_telephone_code'); ?></span> <span class="header__phone-number"><?php echo get_option('site_telephone'); ?></span>
            <a href="#" class="header__call-request">Заказать звонок</a>
          </div>
        </div>
      </div>
	</header><!-- #masthead -->
  <?php
  if( get_field('slider_slug') ) {
      $slider_shortcode = get_field('slider_slug');
      putRevSlider($slider_shortcode, "homepage");
  }
  ?>
	<div id="content" class="site-content">
