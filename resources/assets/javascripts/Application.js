//VENDORS
const $ = window.jQuery || window.$ || {}
require('script-loader!../../../node_modules/slick-carousel/slick/slick');


class Application{
    constructor(){
        this._init();
    }

    _init(){
      this._mainServiceTabsLoad();
      //this._mainPortfolioTabsLoad();
      this._innerTabs();
    }

    _mainServiceTabsLoad(){

      let $firstItemOfTabs = $('.js-services-ajax:first');
      let firstItemIdOfTabs = $firstItemOfTabs.attr('data-id');
      let firstTabType = $firstItemOfTabs.attr('data-type-tab');
      let post_type = $firstItemOfTabs.attr('data-type')
      let $loader = $('.services-content-loader');
      let $tabContent = $('.services-content');

      this._mainTabsAjax($firstItemOfTabs, firstItemIdOfTabs, post_type, $tabContent, $loader, this._slickSliderInit);

      //Click event
      $(document).on('click', '.js-services-ajax', event => {
        event.preventDefault();
        let $this = $(event.currentTarget);
        let id_post = $this.attr('data-id');
        let tab_type = $this.attr('data-type-tab');
        let post_type = $this.attr('data-type');

        $tabContent.empty();
        $loader.fadeIn();
        $('.js-services-ajax').removeClass('active');

        this._mainTabsAjax($this, id_post, post_type, $tabContent, $loader, this._slickSliderInit);
      })
    }

    _mainPortfolioTabsLoad(){
      let $firstItemOfTabs = $('.js-portfolio-ajax:first');
      let firstItemIdOfTabs = $firstItemOfTabs.attr('data-id');
      let post_type = $firstItemOfTabs.attr('data-type')
      let $loader = $('.portfolio-content-loader');
      let $tabContent = $('.portfolio-content');

      this._mainTabsAjax($firstItemOfTabs, firstItemIdOfTabs, post_type, $tabContent, $loader);

      // Click event
      $(document).on('click', '.js-portfolio-ajax', event => {
        event.preventDefault();
        let $this = $(event.currentTarget);
        let id_post = $this.attr('data-id');
        let tab_type = $this.attr('data-type-tab');
        let post_type = $this.attr('data-type');

        $tabContent.empty();
        $loader.fadeIn();
        $('.js-portfolio-ajax').removeClass('active');

        this._mainTabsAjax($this, id_post, post_type, $tabContent, $loader);
      })
    }

    _mainTabsAjax(item, tabId, postType, tabContent, loader, callback){
      $.ajax({
        type: "POST",
        url: '/wp-admin/admin-ajax.php',
        data: {
          action: 'my_load_ajax_content',
          postid: tabId,
          post_type: postType,
        },
        success: result => {
          //console.log(result);
          item.addClass('active');
          tabContent.html(result);
          loader.fadeOut();
          if(callback){
            callback();
          }
        },
        error: function () {
          console.log("error");
        }
      });
    }

    // _innerTabsAjax(){
    //   let $firstItemInnerTabs;
    //   let firstItemId;
    //   let postType;
    //   let tabType;
    //   let dataShortcode;
    //   let contentContainer;
    //
    //   if($('.portfolio-address-tabs__item')){
    //     $firstItemInnerTabs = $('.portfolio-address-tabs__item:first');
    //     console.log($firstItemInnerTabs)
    //     firstItemId = $firstItemInnerTabs.attr('data-id');
    //     postType = $firstItemInnerTabs.attr('data-type');
    //     tabType = $firstItemInnerTabs.attr('data-type-tab');
    //     dataShortcode = $firstItemInnerTabs.attr('data-shortcode');
    //     $firstItemInnerTabs.addClass('active');
    //     contentContainer = $firstItemInnerTabs.parents('.portfolio-content__info').find('.portfolio-address-content');
    //     console.log(contentContainer);
    //     contentContainer.fadeOut(500, function() {
    //       $(this).empty().show();
    //     });
    //   }
    //
    //   $.ajax({
    //     type: "POST",
    //     url: '/wp-admin/admin-ajax.php',
    //     data: {
    //       action: 'my_load_ajax_content',
    //       postid: firstItemId,
    //       post_type: postType,
    //       tab_type: tabType,
    //       data_shortcode: dataShortcode
    //     },
    //     success: function (data) {
    //       contentContainer.fadeIn(500, event => {
    //         console.log(event);
    //         contentContainer.html(data);
    //       });
    //     }
    //   });
    //
    //   $(document).on('click', '.portfolio-address-tabs__item', function (event) {
    //     event.preventDefault();
    //
    //     $('.portfolio-address-tabs__item').removeClass('active');
    //     let $this = $(this);
    //     $this.addClass('active');
    //
    //     let itemId = $this.attr('data-id');
    //     let postType = $this.attr('data-type');
    //     let tabType = $this.attr('data-type-tab');
    //     let dataKey = $this.attr('data-key');
    //     let dataShortcode = $this.attr('data-shortcode');
    //
    //     let contentContainer = $this.parents('.portfolio-content__info').find('.portfolio-address-content');
    //
    //     contentContainer.fadeOut(500, function() {
    //       $(this).empty().show();
    //     });
    //
    //     $.ajax({
    //       type: "POST",
    //       url: '/wp-admin/admin-ajax.php',
    //       data: {
    //         action: 'my_load_ajax_content',
    //         postid: itemId,
    //         post_type: postType,
    //         tab_type: tabType,
    //         data_key: dataKey,
    //         data_shortcode: dataShortcode
    //       },
    //       success:  data => {
    //         contentContainer.fadeIn(500, event => {
    //           console.log(event);
    //           contentContainer.html(data);
    //         });
    //       },
    //       error: function () {
    //         console.log("error");
    //       }
    //     });
    //   });
    // }

    _innerTabs(){
      $(document).on('click', '.portfolio-address-tabs__item, .services-tabs__item, .js-tabs', function(){

        let $this = $(this);

        console.log($this);

        let tab_id = $this.attr('data-tab');
        console.log(tab_id)

        let post_type = $this.attr('data-type');
        console.log(post_type)

        let tab_type = $this.attr('data-type-tab');
        console.log(tab_type)

        if(tab_type === 'main'){

          $this.parents('.base-tabs').find('button').removeClass('active');
          $this.parents('.base-tabs').siblings('.base-tabs-content').removeClass('active');

          $this.addClass('active');
          $('#'+tab_id).addClass('active');

        }else{

          $this.parents('.base-tabs-content').find('button').removeClass('active');
          $this.parents('.base-tabs-content').find('.' + post_type + '-tabs-content').removeClass('active');

          $this.addClass('active');
          $('#'+tab_id).addClass('active');

        }
      })
    }

    _slickSliderInit(){
        $('.slick-carousel').slick({
          dots: true,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
          adaptiveHeight: true
        });
    }
}

new Application();
