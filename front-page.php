<?php get_header(); ?>
    <section class="section" style="background-image: url(<?php echo get_option('bg_section_change'); ?>)">
        <div class="container">
            <h2>Услуги компании</h2>
            <div class="row">
                <div class="col-md-12">
                    <?php $services = new WP_Query( array( 'post_type' => 'services', 'posts_per_page' => '-1', 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
                    <div class="base-tabs">
                      <?php while ( $services->have_posts() ) : $services->the_post(); ?>
                            <button class="base-tabs__item js-services-ajax" data-id="<?php the_ID(); ?>" data-type="<?php echo get_post_type(); ?>" data-type-tab="main"><span><?php the_title(); ?></span></button>
                      <?php endwhile; ?>
                    </div>
                    <div class="base-tabs-content">
                      <div class="services-content">

                      </div>
                      <div class="base-tabs-content__loader services-content-loader">
                        <img src="<?php echo get_template_directory_uri() . '/images/loader.gif' ?>" alt="">
                      </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
      <div class="container">
        <h2>Портфолио компании</h2>
        <div class="row">
          <div class="col-md-12">
            <?php $portfolio = new WP_Query( array( 'post_type' => 'portfolio', 'posts_per_page' => '-1', 'orderby' => 'name', 'order' => 'ASC' ) ); ?>
              <div class="base-tabs">
                <?php while ( $portfolio->have_posts() ) : $portfolio->the_post(); $count++; $index = $portfolio->current_post + 1; ?>
                  <button class="base-tabs__item js-tabs <?= ($index === 1) ? 'active' : '' ?>" data-tab="portfolio-main-tab-id-<?= $index; ?>" data-type="<?php echo get_post_type(); ?>" data-type-tab="main"><span><?php the_title(); ?></span></button>
                <?php endwhile; ?>
              </div>
              <?php while ( $portfolio->have_posts() ) : $portfolio->the_post(); $count++; $index = $portfolio->current_post + 1; ?>
                <div class="base-tabs-content portfolio-main-tabs-content <?= ($index === 1) ? 'active' : '' ?>" id="portfolio-main-tab-id-<?= $index; ?>">
                  <div class="portfolio-content">
                    <?php get_template_part('template-parts/portfolio', 'tab-content') ?>
                  </div>
<!--                  <div class="base-tabs-content__loader portfolio-content-loader">-->
<!--                    <img src="--><?php //echo get_template_directory_uri() . '/images/loader.gif' ?><!--" alt="">-->
<!--                  </div>-->
                </div>
              <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        </div>
		<h2 class="mt-4">Сертификаты и награды</h2>
        <div class="section__text">
          Подробное описание вкладки с регалиями. Тут можно разместить информацию о том, где успела поработать наша компания.Можно написать подробнее о каждой регалии котрую мы получили, а также тут можно разместить текст для сео и просто
          написать, что-нибудь интересное человеку который будет искать информацию о компании ООО “АМ СТРОЙ”. Желательно,
          чтобы текст который будет тут размещен, занимал достаточное крличество пространства, чтобы он не выглядел куцо и в тоже
          время дополнял страницу и делал ее более целостной.
        </div>
        <div class="rewards-block mt-5">
          <?= do_shortcode('[envira-gallery id="152"]'); ?>
        </div>
      </div>
    </section>
    <section class="section" style="background-image: url(<?= get_template_directory_uri() . '/images/map-bg.jpg' ?>)">
      <div class="container">
        <h2>Контакты</h2>
        <div class="row">
          <div class="col-md-4">
            <div class="contact-info-block">
              <div class="contact-info-block__company-name">
                  <?= get_option('company_name'); ?>
              </div>
              <div class="contact-info-block__row">
                  <?= get_option('company_address'); ?>
              </div>
              <div class="contact-info-block__row">
                Адрес электройнной почты:<br/>
                  <?= get_option('company_email'); ?>
              </div>
              <div class="contact-info-block__row">
                Телефон для связи<br/>
                <span class="contact-info-block__phone-code">
                  <?= get_option('site_telephone_code'); ?>
                </span>
                <span class="contact-info-block__phone-number">
                <?= get_option('site_telephone'); ?>
              </span>
              </div>
              <div class="contact-info-block__docs">
                ОКПО: 87688870<br/>
                ИНН: 7703673732<br/>
                КПП: 501801001<br/>
                ОГРН: 5087746023382
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php
get_footer();
